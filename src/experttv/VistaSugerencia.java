/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experttv;

import java.net.URL;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Maudev
 */
public class VistaSugerencia extends javax.swing.JFrame {

    /**
     * Creates new form vistaSugerencia
     */
    private ArrayList<String> memoriaTrabajo = new ArrayList<String>();
    private ArrayList<String> lista = new ArrayList<String>();
    private MotorInferencia motor = new MotorInferencia();
    public VistaSugerencia() {
       
        initComponents();  
        jTable1.getTableHeader().setReorderingAllowed(false);
    }

    public VistaSugerencia(ArrayList<String> memoriaTrabajo) {
        this.memoriaTrabajo = memoriaTrabajo;
        for(int i =0; i < memoriaTrabajo.size(); i++){
            System.out.println(memoriaTrabajo.get(i).toUpperCase());
            switch(memoriaTrabajo.get(i).toUpperCase().trim()){
                case "NOTICIAS":
                    motor.ejecutarPreg("NOTICIAS");  
                    motor.run();
                    break;
                case "INFORMATIVO":
                    motor.ejecutarPreg("INFORMATIVO");  
                    motor.run();
                    break;
                case "DEPORTES":
                    motor.ejecutarPreg("DEPORTES");  
                    motor.run();
                    break;
                case "COMPETENCIAS":
                    motor.ejecutarPreg("COMPETENCIA");  
                    motor.run();
                    break;
                case "FUTBOL":
                    motor.ejecutarPreg("FUTBOL");  
                    motor.run();
                    break;
                case "SERIES":
                    motor.ejecutarPreg("SERIES");  
                    motor.run();
                    break;
                case "NACIONAL":
                    motor.ejecutarPreg("NACIONAL");  
                    motor.run();
                    break;
                case "TELENOVELAS":
                    motor.ejecutarPreg("TELENOVELAS");  
                    motor.run();
                    break;
                case "INTERNACIONAL":
                    motor.ejecutarPreg("INTERNACIONAL");  
                    motor.run();
                    break;
                case "DEPORTIVAS":
                    motor.ejecutarPreg("DEPORTIVAS");  
                    motor.run();
                    break;
                case "POLITICA BOLIVIANA":
                    motor.ejecutarPreg("POLITICA_BOLIVIANA");  
                    motor.run();
                    break;
                case "UNIVERSITARIAS":
                    motor.ejecutarPreg("UNIVERSITARIAS");  
                    motor.run();
                    break;
                case "MUSICAL":
                    motor.ejecutarPreg("MUSICAL");  
                    motor.run();
                    break;
                case "MUSICALES":
                    motor.ejecutarPreg("MUSICALES");  
                    motor.run();
                    break;
                case "JUVENIL":
                    motor.ejecutarPreg("JUVENIL");  
                    motor.run();
                    break;
                case "ROMANTICA":
                    motor.ejecutarPreg("ROMANTICA");  
                    motor.run();
                    break;
                case "POPULACHAS":
                    motor.ejecutarPreg("POPULACHAS");  
                    motor.run();
                    break;
                case "NARCONOVELA":
                    motor.ejecutarPreg("NARCONOVELA");  
                    motor.run();
                    break;
                case "INFANTIL":
                    motor.ejecutarPreg("INFANTIL");  
                    motor.run();
                    break;
                case "DEPORTES_C":
                    motor.ejecutarPreg("DEPORTES_C");  
                    motor.run();
                    break;
                case "MAYORES":
                    motor.ejecutarPreg("MAYORES");  
                    motor.run();
                    break;
                case "ANIMADAS":
                    motor.ejecutarPreg("ANIMADAS");  
                    motor.run();
                    break;
                case "ELECTRONICA":
                    motor.ejecutarPreg("ELECTRONICA");  
                    motor.run();
                    break;
                case "ROCK":
                    motor.ejecutarPreg("ROCK");  
                    motor.run();
                    break;
                case "ANIME":
                    motor.ejecutarPreg("MANGA_ANIME");  
                    motor.run();
                    break;
                case "TROPICAL":
                    motor.ejecutarPreg("TROPICAL");  
                    motor.run();
                    break;
                case "NO_ANIMADAS":
                    motor.ejecutarPreg("NO_ANIMADAS");  
                    motor.run();
                    break;
                case "RECUERDO":
                    motor.ejecutarPreg("RECUERDO");  
                    motor.run();
                    break;
                case "MUSICA_DANZA":
                    motor.ejecutarPreg("MUSICA_DANZA");  
                    motor.run();
                    break;
                case "ACCION_FICCION":
                    motor.ejecutarPreg("ACCION_FICCION");  
                    motor.run();
                    break;
                case "ENTRETENIMIENTO VARIADO":
                    motor.ejecutarPreg("ENTRETENIMIENTO_VARIADO");  
                    motor.run();
                    break;
                case "EN_VIVO":
                    motor.ejecutarPreg("EN_VIVO");  
                    motor.run();
                    break;
                case "PELICULAS":
                    motor.ejecutarPreg("PELICULAS");  
                    motor.run();
                    break;
                case "TODO PUBLICO":
                    motor.ejecutarPreg("TODO_PUBLICO");  
                    motor.run();
                    break;
                case "COMEDIA":
                    motor.ejecutarPreg("COMEDIA");  
                    motor.run();
                    break;
                case "ACCION":
                    motor.ejecutarPreg("ACCION");  
                    motor.run();
                    break;
                case "LUCHAS":
                    motor.ejecutarPreg("LUCHAS");  
                    motor.run();
                    break; 
                case "MARTES_MIERCOLES":
                    motor.ejecutarPreg("MARTES_MIERCOLES");  
                    motor.run();
                    break;
                case "JUEVES_VIERNES":
                    motor.ejecutarPreg("JUEVES_VIERNES");  
                    motor.run();
                    break;
                case "LUNES_DOMINGO":
                    motor.ejecutarPreg("LUNES_DOMINGO");  
                    motor.run();
                    break;
                case "LUNES_VIERNES":
                    motor.ejecutarPreg("LUNES_VIERNES");  
                    motor.run();
                    break;
                case "SABADO":
                    motor.ejecutarPreg("SABADO");  
                    motor.run();
                    break;
                case "DOMINGO":
                    motor.ejecutarPreg("DOMINGO");  
                    motor.run();  
                    break;
                case "SABADO_DOMINGO":
                    motor.ejecutarPreg("SABADO_DOMINGO");  
                    motor.run();  
                    break;
                case "MUCHA PROPAGANDA":
                    motor.ejecutarPreg("MUCHA_PROPAGANDA");  
                    motor.run();
                    break;
                case "SIN MUCHA PROPAGANDA":
                    motor.ejecutarPreg("SIN_MUCHA_PROPAGANDA"); 
                    motor.run();
                    break;   
                case "HORARIO DIURNO":
                    motor.ejecutarPreg("HORARIO_DIURNO"); 
                    motor.run();
                    break;
                case "HORARIO NOCTURNO":
                    motor.ejecutarPreg("HORARIO_NOCTURNO"); 
                    motor.run();
                    break;
                 
            }       
        }
        
        initComponents();
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.setDefaultRenderer(Object.class,new IconCellRenderer());
        DefaultTableModel modelo=(DefaultTableModel) jTable1.getModel(); 
        StringBuilder str = new StringBuilder();
        for(String s: memoriaTrabajo)
            str.append(s+"\n");
        jTextArea1.setText(str.toString());
        lista = motor.hechos();
        StringBuilder str2 = new StringBuilder(); 
        ArrayList<String> tableValues = valueTokenizer(lista.get(lista.size()-1));
        ArrayList<String> row = new ArrayList<String>();
        for(String s: lista)
            System.out.println("val: "+s);
        for(int i = 0; i < tableValues.size(); i++){
            //System.out.println("val: "+tableValues.get(i));
            //aux[aux.length-1] = createImage("images/canales/atb.png");           
            if(tableValues.get(i).equals("-")){
                Object[] aux = row.toArray();
                ImageIcon icon1;
                switch(tableValues.get(i-1)){
                    case "ATB":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/atb.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "RED UNO":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/red-uno.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "BOLIVISION":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/bolivision.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "BTV":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/btv.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "CANAL 2":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/cca.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "PAT":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/pat.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "TELE C":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/telec.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "TVU":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/tvu.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "UNITEL":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/unitel.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                    case "UNIVALLE":
                        icon1 = new ImageIcon(getClass().getResource("/images/canales/univalle.png"));
                        aux[aux.length-1] = new JLabel(icon1);
                        modelo.addRow(aux);
                        jTable1.setModel(modelo);
                        row.removeAll(row);
                        break;
                }
            }else row.add(tableValues.get(i));
            
            
        }
        //jTable1.setModel(modelo); 
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    public ImageIcon createImage(String path) {
        URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
          System.err.println("La imagen existe en: " + path);
          return new ImageIcon(imgURL);
        } else {
             System.err.println("Couldn't find file: " + path);
             return null;
        }
    }
    private ArrayList<String> valueTokenizer(String p){
        char x = 'x';
        ArrayList<String> res = new ArrayList<String>();
        StringBuilder str = new StringBuilder();
        for(int i = 0; i <= p.length()-1; i++){   
            x = p.charAt(i);
        switch(x){
            case '%':
                res.add(str.toString());    
                str.setLength(0);
                break;
            case '*':        
                res.add(str.toString());
                res.add("-"); 
                str.setLength(0);
                break;
            case '-':
                res.add(str.toString());
                res.add("$"); 
                str.setLength(0);
                break;
            default:
                str.append(x);
                break;
        }
        }
        res.add(str.toString());   
        return res;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Expert TV");
        setMinimumSize(new java.awt.Dimension(800, 600));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/app/splash2.png"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, -20, 220, 210));

        jLabel5.setFont(new java.awt.Font("Lucida Bright", 3, 30)); // NOI18N
        jLabel5.setText("Sugerencia de programación televisiva");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 610, 90));

        jLabel8.setFont(new java.awt.Font("Lucida Bright", 2, 12)); // NOI18N
        jLabel8.setText("Sistema experto Expert TV para programación nacional..");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        jLabel1.setFont(new java.awt.Font("Lucida Bright", 2, 24)); // NOI18N
        jLabel1.setText("Usted puede sintonizar:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 530, 60));

        jButton1.setText("Volver al menú principal");
        jButton1.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 460, 230, -1));

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 180, 230, 200));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Programa", "Horario", "Descripcion", "Canal"
            }
        ));
        jTable1.setRowHeight(72);
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 540, 430));

        jLabel2.setText("Memoria de trabajo:");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 160, 120, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/app/background.jpg"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 600));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // MENU PRINCIPAL
        
        memoriaTrabajo.removeAll(memoriaTrabajo);
        VistaPrincipal obj1 = new VistaPrincipal(memoriaTrabajo);
        obj1.setVisible(true);
        dispose();   
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaSugerencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaSugerencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaSugerencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaSugerencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaSugerencia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
