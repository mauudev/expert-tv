/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experttv;

/**
 *
 * @author Maudev
 */
import java.util.ArrayList;
import java.util.Iterator;
import jess.Fact;
import jess.JessException;
import jess.Rete;
import jess.Value;

public class MotorInferencia {
	
	private static ArrayList<String> preguntaElegida = new ArrayList<String>();
	private ArrayList<String> memoriaTrabajo = new ArrayList<String>();
	private ArrayList<String> descripcion = new ArrayList<String>();
	private Rete rete = new Rete();
        
	public MotorInferencia(){
		abrir();   
	}
	
	public void ejecutarPreg(String p1) {
		try {
                        //System.out.println("Pregunta: " + p1);
			rete.eval("(assert("+p1+"))");
                        
                        //Value v=r.eval(p1);
                        //System.out.println(v.stringValue(r.getGlobalContext()));
		} catch (JessException e) {
			e.printStackTrace();
		}
	}
	
	public void abrir() {
		try {
			 rete.batch("experttv/BaseConocimiento.clp");
			 rete.reset();
		} catch (JessException e) {
            e.printStackTrace();
		}
	}
	public void run() {
		try {
			rete.run();
		} catch (JessException e) {
			e.printStackTrace();
		}
	}
	

    /**
     * @param args
     */
    @SuppressWarnings("unchecked")
    public  void mostrarMT() {
    // TODO Auto-generated method stub
        Iterator it = rete.listFacts();
    	while (it.hasNext()) {	
          System.out.println(it.getClass().toString());
          memoriaTrabajo.add( it.next().toString()+ "\nl");
	}
    
    }
    protected ArrayList<String> getMt() {
	return memoriaTrabajo;
    }

    protected void setMt(ArrayList<String>mt) {
	this.memoriaTrabajo = mt;
    }


    public void mostrarConclusion(){
//        try {
//            //            Value v=r.fetch("CRISTO DE LA CONCORDIA");
//            //            System.out.println(v.stringValue(r.getGlobalContext()));
//                        
//                        System.out.println(r.fetch("RESULT").stringValue(r.getGlobalContext()));
//        } catch (JessException ex) {
//            Logger.getLogger(MotorInferencia.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
    }
    public void remo() throws JessException
    {
        
        //r.removeFacts(a);
        rete.reset();
    }
    
    public ArrayList<String> hechos(){
		ArrayList<String> hechosContenidos = new ArrayList<String>();
		
		try{
			Iterator<Fact> it = rete.listFacts();
			while( it.hasNext() ){
				Fact h = rete.findFactByFact(it.next());
				hechosContenidos.add(h.toString());
                                //System.out.println("Hecho: "+h);
			}
		}catch(Exception ex){}
		
		ArrayList<String> res = new ArrayList<String>();
		
		for( String s : hechosContenidos ){
			String hecho = s.substring(7, s.length()-1);
			res.add(hecho);
		}
		return res;
	}
    
    
    
	
}

