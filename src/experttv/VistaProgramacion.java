/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experttv;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Maudev
 */
public class VistaProgramacion extends javax.swing.JFrame {
    private ArrayList<String> memoriaTrabajo = new ArrayList<String>();
    /**
     * Creates new form vistaProgramacion
     */
    public VistaProgramacion() {
        initComponents();
        
    }
    public VistaProgramacion(ArrayList<String> memoriaTrabajo) {
        initComponents();
        this.memoriaTrabajo = memoriaTrabajo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jRadioButton7 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton5 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/app/background.jpg"))); // NOI18N

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Expert TV");
        setPreferredSize(new java.awt.Dimension(800, 600));
        getContentPane().setLayout(null);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/app/splash2.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(550, -20, 220, 210);

        jLabel2.setFont(new java.awt.Font("Lucida Bright", 3, 30)); // NOI18N
        jLabel2.setText("Sugerencia de programación televisiva");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, -10, 650, 90);

        jLabel3.setFont(new java.awt.Font("Lucida Bright", 0, 18)); // NOI18N
        jLabel3.setText("Por favor, seleccione el tipo de programación que desea sintonizar:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 110, 770, 40);

        jButton3.setText("Menú principal");
        jButton3.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(510, 530, 120, 23);

        jButton4.setText("Siguiente");
        jButton4.setPreferredSize(new java.awt.Dimension(80, 23));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(640, 530, 90, 23);

        jRadioButton7.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton7);
        jRadioButton7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton7.setText("Telenovelas");
        jRadioButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton7ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton7);
        jRadioButton7.setBounds(650, 350, 117, 31);

        jRadioButton2.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton2.setText("Noticias");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton2);
        jRadioButton2.setBounds(460, 350, 90, 31);

        jRadioButton1.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton1.setText("Deportes");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton1);
        jRadioButton1.setBounds(460, 300, 95, 31);

        jRadioButton3.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton3);
        jRadioButton3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton3.setText("Series");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton3);
        jRadioButton3.setBounds(560, 300, 71, 31);

        jRadioButton5.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton5);
        jRadioButton5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton5.setText("Peliculas");
        jRadioButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton5);
        jRadioButton5.setBounds(650, 300, 91, 31);

        jRadioButton4.setBackground(new java.awt.Color(153, 255, 153));
        buttonGroup1.add(jRadioButton4);
        jRadioButton4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jRadioButton4.setText("Musical");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton4);
        jRadioButton4.setBounds(560, 350, 83, 31);

        jLabel6.setFont(new java.awt.Font("Lucida Bright", 2, 12)); // NOI18N
        jLabel6.setText("Sistema experto Expert TV para programación nacional..");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(10, 50, 590, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/app/watch.png"))); // NOI18N
        getContentPane().add(jLabel7);
        jLabel7.setBounds(20, 170, 170, 160);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setPreferredSize(new java.awt.Dimension(170, 100));
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 370, 200, 170);

        jLabel8.setText("Memoria de trabajo:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(10, 340, 120, 30);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/canales/nacionales-canales.png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(220, 170, 170, 160);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/programas/noticieros/deportes.png"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(230, 360, 180, 180);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // DEPORTES
        memoriaTrabajo.add(jRadioButton1.getText());
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        //Boton atras
        memoriaTrabajo.removeAll(memoriaTrabajo);
        VistaPrincipal obj1 = new VistaPrincipal(memoriaTrabajo);
        obj1.setVisible(true);
        dispose();  
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
       //SERIES
       memoriaTrabajo.add(jRadioButton3.getText());
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void jRadioButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton5ActionPerformed
       //PELICULAS
       memoriaTrabajo.add(jRadioButton5.getText());
    }//GEN-LAST:event_jRadioButton5ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // NOTICIAS
        memoriaTrabajo.add(jRadioButton2.getText());
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        // MUSICALES
        memoriaTrabajo.add("MUSICALES");
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jRadioButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton7ActionPerformed
        // TELENOVELAS
        memoriaTrabajo.add(jRadioButton7.getText());
    }//GEN-LAST:event_jRadioButton7ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // Boton siguiente
        try{
        switch(memoriaTrabajo.get(0)){
            case "Deportes":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaDeportesTipo obj1 = new VistaDeportesTipo(memoriaTrabajo);
                obj1.setVisible(true);
                dispose();
                break;
            case "Documentales":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaNoticiasTipo obj2 = new VistaNoticiasTipo(memoriaTrabajo);
                obj2.setVisible(true);
                dispose();
                break;
            case "Series":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaSeriesTipo obj3 = new VistaSeriesTipo(memoriaTrabajo);
                obj3.setVisible(true);
                dispose();
                break;
            case "Noticias":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaNoticiasTipo obj4 = new VistaNoticiasTipo(memoriaTrabajo);
                obj4.setVisible(true);
                dispose();
                break;
            case "Peliculas":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaPeliculasDiasSD obj5 = new VistaPeliculasDiasSD(memoriaTrabajo);
                obj5.setVisible(true);
                dispose();
                break;
            case "Telenovelas":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaNovelasTipo obj6 = new VistaNovelasTipo(memoriaTrabajo);
                obj6.setVisible(true);
                dispose();
                break;
            case "MUSICALES":
                System.out.println("Texto: "+memoriaTrabajo.get(0));
                VistaMusicalAudiencia obj7 = new VistaMusicalAudiencia(memoriaTrabajo);
                obj7.setVisible(true);
                dispose();
                break;
            default:
                System.out.println("Debe elegir una categoria");
                break;
        }
        }catch(IndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(null,"Debe seleccionar una categoria !","Error",JOptionPane.WARNING_MESSAGE);
        }
        
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaProgramacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaProgramacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaProgramacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaProgramacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaProgramacion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JRadioButton jRadioButton7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
