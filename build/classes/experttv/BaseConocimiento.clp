;--------------NOTICIAS---------------
;-------------- REGLA 1 --------------
(defrule rule1
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (INTERNACIONAL)
	 ?p3 <- (HORARIO_DIURNO)
	 =>
	 (assert (BTV Internacional%8:30 am%Noticias de la actualidad internacional transmitido por el canal de los bolivianos.%BTV*TVU Enlace Internacional%12:00 pm%Las noticias de enlace internacional, en la pantalla de TVU Enlace Euronews%TVU*))
)
;-------------- REGLA 2 --------------
(defrule rule2
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (INTERNACIONAL)
	 ?p3 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (BTV Internacional%19:30 pm%Noticias de la actualidad internacional transmitido por el canal universitario de la UMSS%TVU*))
)

;-------------------------------------

;-------------- REGLA 4 --------------
(defrule rule4
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (DEPORTIVAS)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (A todo deporte%12:00 pm%Todas las noticias del deporte por el canal UNITEL%UNITEL*Deporte total%12:20 pm%Las noticias del deporte por ATB%ATB*El super deportivo%12:20 pm%Noticias deportivas por el canal naranja Red UNO%RED UNO*Teledeportivo%12:00 pm%Todo las noticias del deporte por TELE C%TELE C*))
)
;-------------------------------------
;-------------- REGLA 5 --------------
(defrule rule5
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (DEPORTIVAS)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (Deporte total%18:40 pm%Noticias deportivas en horario nocturno por ATB%ATB*El super deportivo%24:30 am%Las noticias deportivas nocturnas por RED UNO%RED UNO*))
)
;-------------------------------------
;-------------- REGLA 6 --------------
(defrule rule6
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (DEPORTIVAS)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (Analisis arbitral%12:30 pm%Noticias deportivas por Bolivia TV%BTV*JNN Deportes%12:00 pm%Las noticias del deporte por CCA%CANAL 2*))
)
;-------------------------------------
;-------------- REGLA 7 --------------
(defrule rule7
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (DEPORTIVAS)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (El deportivo%21:00 pm%La actualidad deportiva de Bolivia, el mundo y de la UMSS%TVU*JNN Deportes%12:00 pm%Las noticias del deporte por CCA%CANAL 2*))
)
;-------------------------------------

;-------------- REGLA 8 --------------
(defrule rule8
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (UNIVERSITARIAS)
	 =>
	 (assert (Revista universitaria%7:00 am%Noticias de la UMSS por TVU%TVU*De frente/buenas noticias%18:30 pm%Boletines informativos de la UMSS%TVU*FUL Informa%20:00 pm%Boletin informativo de la FUL%TVU*))
)
;-------------------------------------
;-------------- REGLA 9 --------------
(defrule rule9
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (NACIONAL)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (Notivision%19:45 pm%Noticiero nocturno por la RED UNO%RED UNO*Telepais%19:45 pm%Telepais nocturno, toda las noticias de la actualidad por UNITEL%UNITEL*Bolivision al dia%20:35 pm%Bolivision al dia con las noticias de la actualidad nacional e internacional.%BOLIVISION*ATB Noticias%19:30 pm%ATB Noticias con todas las noticias de la actualidad.%ATB*))
)
;-------------------------------------
;-------------- REGLA 10 --------------
(defrule rule10
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (NACIONAL)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (La revista%06:00 am%Toda la informacion, humor y noticias por la revista de UNITEL%UNITEL*Notivision%12:45 pm%El mejor noticiero de Bolivia solo por RED UNO.%RED UNO*Telepais%12:53 pm%Las noticias de la actualidad nacional e internacional por UNITEL.%UNITEL*Bolivision al dia%06:00am%Toda la actualidad por BOLIVISION.%BOLIVISION*ATB Noticias%6:30 am%Noticiero edicion matutina por ATB.%ATB*El mañanero%06:00 am%Noticias, humor y actualidad por RED UNO.%RED UNO*))       
)
;-------------------------------------
;-------------- REGLA 11 --------------
(defrule rule11
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (NACIONAL)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (TVU Enlace nacional%12:30 pm%La actualidad nacional por el canal universitario.%TVU*Telenoticias%12:45 pm%Telenoticias con toda la informacion nacional por TELE C%TELE C.*JNN Noticias%12:00 pm%La actualidad nacional solo por CCA, Cochabamba corazon de america%CANAL 2*))
)
;-------------------------------------
;-------------- REGLA 12 --------------
(defrule rule12
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (NACIONAL)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (TVU Enlace nacional%20:00 pm%Noticiero edicion nocturna por TVU.%TVU*Telenoticias%20:00 pm%Telenoticiero en su edicion nocturna solo por TELE C.%TELE C*JNN Noticias%20:00 pm%La actualidad nacional solo por CCA.%CANAL 2*))
)
;-------------------------------------
;-------------- REGLA 13 --------------
(defrule rule13
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (POLITICA_BOLIVIANA)
	 ?p3 <- (HORARIO_DIURNO)
	 =>
	 (assert (BTV Noticiero%10:00 am, 13:00 pm%Las noticias de la actualidad del Estado Plurinacional de Bolivia, solo por Bolivia TV%BTV*Los movimientos sociales hacen noticia%9:55 am,12:55 pm, 16:25 pm%Boletines informativos de nuestra Bolivia por Bolivia TV%BTV*))
)
;--------------------------------------
;-------------- REGLA 14 --------------
(defrule rule14
	 ?p1 <- (NOTICIAS)
	 ?p2 <- (POLITICA_BOLIVIANA)
	 ?p3 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (BTV Noticiero%20:00 pm, 23:50 pm%Las noticias de la actualidad Boliviana por Bolivia TV%BTV*))
)
;--------------NOTICIAS----------------

;--------------DEPORTES----------------
;-------------- REGLA 15 --------------
(defrule rule15
	 ?p1 <- (DEPORTES)
	 ?p2 <- (INFORMATIVO)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (A todo deporte%12:00 pm%Todas las noticias del deporte por el canal UNITEL%UNITEL*Deporte total%12:20 pm%Las noticias del deporte por ATB%ATB*El super deportivo%12:20 pm%Noticias deportivas por el canal naranja Red UNO%RED UNO*Teledeportivo%12:00 pm%Todo las noticias del deporte por TELE C%TELE C*))
)
;-------------------------------------
;-------------- REGLA 16 --------------
(defrule rule16
	 ?p1 <- (DEPORTES)
	 ?p2 <- (INFORMATIVO)
	 ?p3 <- (MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (Deporte total%18:40 pm%Noticias deportivas en horario nocturno por ATB%ATB*El super deportivo%24:30 am%Las noticias deportivas nocturnas por RED UNO%RED UNO*))
)
;-------------------------------------
;-------------- REGLA 17 --------------
(defrule rule17
	 ?p1 <- (DEPORTES)
	 ?p2 <- (INFORMATIVO)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (Analisis arbitral%12:30 pm%Noticias deportivas por Bolivia TV%BTV*JNN Deportes%12:00 pm%Las noticias del deporte por CCA%CANAL 2*))
)
;-------------------------------------
;-------------- REGLA 18 --------------
(defrule rule18
	 ?p1 <- (DEPORTES)
	 ?p2 <- (INFORMATIVO)
	 ?p3 <- (SIN_MUCHA_PROPAGANDA)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (El deportivo%21:00 pm%La actualidad deportiva de Bolivia, el mundo y de la UMSS%TVU*JNN Deportes%12:00 pm%Las noticias del deporte por CCA%CANAL 2*))
)
;-------------------------------------
;-------------- REGLA 19 --------------
(defrule rule19
	 ?p1 <- (DEPORTES)
	 ?p2 <- (LUCHAS)
	 ?p3 <- (SABADO)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (RAW Lucha libre%16:00 pm%Las mejores peleas de lucha libre, solo en RAW por BOLIVISION%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 20 --------------
(defrule rule20
	 ?p1 <- (DEPORTES)
	 ?p2 <- (LUCHAS)
	 ?p3 <- (DOMINGO)
	 ?p4 <- (HORARIO_DIURNO)
	 =>
	 (assert (SMACKDOWN Lucha libre%16:00 pm%Las mejores peleas de lucha libre, solo en SMACKDOWN por BOLIVISION%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 21 --------------
(defrule rule21
	 ?p1 <- (DEPORTES)
	 ?p2 <- (LUCHAS)
	 ?p3 <- (SABADO)
	 ?p4 <- (HORARIO_NOCTURNO)
	 =>
	 (assert (UFC Ahora%00:00 am%Las mejores peleas de UFC por RED UNO%RED UNO*))
)
;-------------------------------------
;-------------- REGLA 22 --------------
(defrule rule22
	 ?p1 <- (DEPORTES)
	 ?p2 <- (FUTBOL)
	 ?p3 <- (INTERNACIONAL)
	 ?p4 <- (MARTES_MIERCOLES)
	 =>
	 (assert (UEFA Champions League%Desde las 15:00 pm%Vive los partidos del mejor campeonato del mundo%UNITEL*UEFA Champions League%Desde las 15:00 pm%Vive los partidos del mejor campeonato del mundo%BTV*))
)
;-------------------------------------
;-------------- REGLA 23 --------------
(defrule rule23
	 ?p1 <- (DEPORTES)
	 ?p2 <- (FUTBOL)
	 ?p3 <- (INTERNACIONAL)
	 ?p4 <- (JUEVES_VIERNES)
	 =>
	 (assert (UEFA Europa League%Desde las 14:00 pm%Todos los partidos de la UEFA EUROPA LEAGUE%UNITEL*UEFA Europa League%Desde las 14:00 pm%Todos los partidos de la UEFA EUROPA LEAGUE%BTV*))
)
;-------------------------------------
;-------------- REGLA 24 --------------
(defrule rule24
	 ?p1 <- (DEPORTES)
	 ?p2 <- (FUTBOL)
	 ?p3 <- (INTERNACIONAL)
	 ?p4 <- (LUNES_VIERNES)
	 =>
	 (assert (Copa libertadores de America%Desde las 20:00 pm%Los mejores partidos de los equipos campeones de Sudamerica%UNITEL*))
)
;-------------------------------------
;-------------- REGLA 25 --------------
(defrule rule25
	 ?p1 <- (DEPORTES)
	 ?p2 <- (FUTBOL)
	 ?p3 <- (NACIONAL)
	 ?p4 <- (LUNES_DOMINGO)
	 =>
	 (assert (Liga Profesional de Futbol Boliviano%Desde las 15:00 pm%Los partidos de la liga nacional solo por Bolivia TV%BTV*))
)
;-------------------------------------
;--------------DEPORTES----------------

;--------------SERIES------------------
;-------------- REGLA 26 --------------
(defrule rule26
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (INFANTIL)
	 ?p4 <- (LUNES_VIERNES)
	 =>
	 (assert (Los Simpsons%11:45 am, 16:00 pm%La serie de la familia mas graciosa del mundo solo por UNITEL%UNITEL*
			  ))
)
;-------------------------------------
;-------------- REGLA 27 --------------
(defrule rule27
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (INFANTIL)
	 ?p4 <- (SABADO)
	 =>
	 (assert (Los padrinos magicos%10:30 am%Serie animada para los menores de la casa%ATB*Tortugas Ninjas%07:30 am%Las tortugas ninjas en serie infantil%ATB*Kung Fu Panda%08:00 am%Serie animada para los menores de la casa%ATB*Nonstruos contra alienigenas%15:30 pm%Serie infantil de accion%ATB*Growing up creepie%06:00 am%Animación, Dibujo animado, Comedia cinematográfica%BOLIVISION*Master Raindrop%06:30 am%Master Raindrop sigue el viaje de cuatro jóvenes amigos Raindrop, ShaoYen, Jinhou y Niwa a través de "La tierra de las mil leyendas"%BOLIVISION*Ninja Hatori%07:00 am%Es una serie de manga y anime creada por el mítico dúo de mangakas Fujiko Fujio, que trata sobre las aventuras de un niño ninja llamado Hattori, junto a su hermano Shinzo, su perro ninja Shishimaru y Kenichi.%BOLIVISION*Blue Dragon%07:30 am%Esta es una lista de episodios de la serie de animación Blue Dragon, que está basado en el manga escrito por Ami Shibata y en el videojuego de XBOX360 que tiene el mismo nombre.%BOLIVISION*Doraemon%08:30 am%Es uno de los animes más famosos de la historia, tanto que en Japón muchas veces se ha retransmitido por televisión.%BOLIVISION*Wolverine%09:00 am%El superheroe de los X-Men en serie animada para niños%BOLIVISION*Chavo animado%09:30 am%El clasico de todos los tiempos en serie animada para niños.%BOLIVISION*Megaman%10:00 am%Anime japones de la saga Megaman en serie animada para todo publico.%BOLIVISION*Pink panter%12:00 pm%La pantera rosa el clasico de todos los tiempos en serie animada original.%BOLIVISION*Pucca%09:00 am%La popular serie para ninos en UNITEL.%UNITEL*Los Simpsons%11:30 am, 17:00 pm%La serie de la familia mas graciosa de la TV%UNITEL*))
)
;-------------------------------------
;-------------- REGLA 28 --------------
(defrule rule28
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (INFANTIL)
	 ?p4 <- (DOMINGO)
	 =>
	 (assert (Los padrinos magicos%10:30 am%Serie animada para los menores de la casa%ATB*Tortugas Ninjas%07:30 am%Las tortugas ninjas en serie infantil%ATB*
			  Kung Fu Panda%08:00 am%Serie animada para los menores de la casa%ATB*Monstruos contra alienigenas%15:30 pm%Serie infantil de accion%ATB*
			  Growing up creepie%06:00 am%Animación, Dibujo animado, Comedia cinematográfica%BOLIVISION*
			  Master Raindrop%06:30 am%Master Raindrop sigue el viaje de cuatro jóvenes amigos Raindrop, ShaoYen, Jinhou y Niwa a través de "La tierra de las mil leyendas"%BOLIVISION*
			  Ninja Hatori%07:00 am%Es una serie de manga y anime creada por el mítico dúo de mangakas Fujiko Fujio, que trata sobre las aventuras de un niño ninja llamado Hattori, junto a su hermano Shinzo, su perro ninja Shishimaru y Kenichi.%BOLIVISION*
			  Blue Dragon%07:30 am%Esta es una lista de episodios de la serie de animación Blue Dragon, que está basado en el manga escrito por Ami Shibata y en el videojuego de XBOX360 que tiene el mismo nombre.%BOLIVISION*
			  Doraemon%08:30 am%Es uno de los animes más famosos de la historia, tanto que en Japón muchas veces se ha retransmitido por televisión.%BOLIVISION*
			  Wolverine%09:00 am%El superheroe de los X-Men en serie animada para niños%BOLIVISION*
			  Chavo animado%09:30 am%El clasico de todos los tiempos en serie animada para niños.%BOLIVISION*
			  Megaman%10:00 am%Anime japones de la saga Megaman en serie animada para todo publico.%BOLIVISION*
			  Pink panter%12:00 pm%La pantera rosa el clasico de todos los tiempos en serie animada original.%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 29 --------------
(defrule rule29
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (TODO_PUBLICO)
	 ?p4 <- (COMEDIA)
	 ?p5 <- (LUNES_VIERNES)
	 =>
	 (assert (Los Simpsons%11:45 am, 16:00 pm%La serie de la familia mas graciosa del mundo solo por UNITEL%UNITEL*))
)
;-------------------------------------
;-------------- REGLA 30 --------------
(defrule rule30
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (TODO_PUBLICO)
	 ?p4 <- (COMEDIA)
	 ?p5 <- (SABADO)
	 =>
	 (assert (Los Simpsons%11:30 am, 17:00 pm%La serie de la familia mas graciosa de la TV%UNITEL*Pink panter%12:00 pm%La pantera rosa el clasico de todos los tiempos en serie animada original.%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 31 --------------
(defrule rule31
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (TODO_PUBLICO)
	 ?p4 <- (COMEDIA)
	 ?p5 <- (DOMINGO)
	 =>
	 (assert (Pink panter%12:00 pm%La pantera rosa el clasico de todos los tiempos en serie animada original.%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 32 --------------
(defrule rule32
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (TODO_PUBLICO)
	 ?p4 <- (ACCION)
	 ?p5 <- (SABADO)
	 =>
	 (assert (Wolverine%09:00 am%El superheroe de los X-Men en serie animada para niños%BOLIVISION*
			  Megaman%10Ñ00 am%Anime japones de la saga Megaman en serie animada para todo publico.%BOLIVISION*
			  Monstruos contra alienigenas%15:30 pm%Serie animada de accion para niños y mayores.%ATB*))
)
;-------------------------------------
;-------------- REGLA 33 --------------
(defrule rule33
	 ?p1 <- (SERIES)
	 ?p2 <- (ANIMADAS)
	 ?p3 <- (TODO_PUBLICO)
	 ?p4 <- (ACCION)
	 ?p5 <- (DOMINGO)
	 =>
	 (assert (Wolverine%09:00 am%El superheroe de los X-Men en serie animada para niños%BOLIVISION*
			  Megaman%10Ñ00 am%Anime japones de la saga Megaman en serie animada para todo publico.%BOLIVISION*
			  Monstruos contra alienigenas%15:30 pm%Serie animada de accion para niños y mayores.%ATB*))
)
;-------------------------------------
;-------------- REGLA 34 --------------
(defrule rule34
	 ?p1 <- (SERIES)
	 ?p2 <- (NO_ANIMADAS)
	 ?p3 <- (COMEDIA)
	 ?p4 <- (LUNES_VIERNES)
	 =>
	 (assert (Chespirito%09:00 am%El clasico chespirito para el entretenimiento de la familia.%BOLIVISION*
			  El chavo del 8%18:30 pm%La serie comica de todos los tiempos, el chavo del 8%BOLIVISION*))
)
;-------------------------------------
;-------------- REGLA 35 --------------
(defrule rule35
	 ?p1 <- (SERIES)
	 ?p2 <- (NO_ANIMADAS)
	 ?p3 <- (COMEDIA)
	 ?p4 <- (SABADO)
	 =>
	 (assert (Dos hombres y medio%10:30 am%Serie comica por ATB*
			  Chespirito%09:00 am%El clasico chespirito para el entretenimiento de la familia.%BOLIVISION*
			  El chavo del 8%18:30 pm%La serie comica de todos los tiempos, el chavo del 8%BOLIVISION*	  
			  Plaza sesamo%08:00 am%La clasica serie de los muñecos locos de BOLIVISION%BOLIVISION*
			  Glee%06:00 am%Serie musical y comedia por UNITEL%UNITEL*
			  Alf%10:00 am%El clasico extraterrestre gracioso y su familia.%UNITEL%UNITEL*
			  El principe del Rap%10:30 am%Serie comica para todo publico con Will Smith%UNITEL%UNITEL*
			  Tres por tres%11:00 am%La clasica serie de la graciosa familia Olsen%UNITEL%UNITEL*
			  La hora de Hannah%18:00 pm%Serie comica protagonizada por Miley Cyrus.%TELE C*
			  Vecinos%17:00 pm%Serie mexicana comica que protagonizan vecinos amigos de barrio%RED UNO*
			  Corre video%14:00 pm%Divertida serie de videos caseros para matarse de risa.%UNITEL*
			  Camara loca%13:00 pm%Divertida serie de videos caseros para matarse de risa.%BOLIVISION*
			  La hora pico%14:30%Serie comica mexicana por Bolivision%BOLIVISION*
			  Lapsus%14:00 pm%Serie de videos caseros divertidos%RED UNO*))
)
;-------------------------------------
;-------------- REGLA 36 --------------
(defrule rule36
	 ?p1 <- (SERIES)
	 ?p2 <- (NO_ANIMADAS)
	 ?p3 <- (COMEDIA)
	 ?p4 <- (DOMINGO)
	 =>
	 (assert (Dos hombres y medio%06:00 am,10:30 am%Serie comica por ATB*			
			  El chavo del 8%14:00 pm%La serie comica de todos los tiempos, el chavo del 8%BOLIVISION*	  
			  Plaza sesamo%08:00 am%La clasica serie de los muñecos locos de BOLIVISION%BOLIVISION*
			  La hora de Hannah%18:00 pm%Serie comica protagonizada por Miley Cyrus.%TELE C*
			  Que familia los serrano%13:00%Los serrano la serie comica boliviana dirigida y producida por Jenny Serrano%RED UNO*
			  Camara loca%13:00 pm%Divertida serie de videos caseros para matarse de risa.%BOLIVISION*
			  La hora pico%14:30%Serie comica mexicana por Bolivision%BOLIVISION*
			  Lapsus%14:00 pm%Serie de videos caseros divertidos%RED UNO*))
)
;-------------------------------------

;-------------- REGLA 37 --------------
(defrule rule37
	 ?p1 <- (SERIES)
	 ?p2 <- (NO_ANIMADAS)
	 ?p3 <- (ACCION_FICCION)
	 ?p4 <- (SABADO)
	 =>
	 (assert (Metástasis%21:00 pm%La serie americana de accion, suspenso y drama basada en la serie de televisión estadounidense Breaking Bad%ATB*
			  The client list%22:30%The Client List es una serie de televisión estadounidense del género dramático, ficcion.%ATB*
			  Sin retorno%23:00 pm%Sin Retorno es una serie de televisión colombiana realizada por Fox Telecolombia donde se plantean situaciones fuera de lo común.%BOLIVISION*
			  Extant%18:00 pm%Serie de ciencia ficción emitida por la cadena CBS. La serie gira en torno a una astronauta que vuelve a casa.%BOLIVISION*
			  Ncis los angeles%19:00 pm%es una serie de televisión de ficción estadounidense sobre un equipo de agentes especiales del Servicio de Investigación Criminal de la Marina de los Estados Unidos que trabajan en Los Ángeles.%BOLIVISION*
			  Hawaii Five0%20:00 pm%Hawaii Five0 es una serie de televisión de crimen y drama estadounidense%BOLIVISION*
			  The walking dead%21:00 pm% La serie se sitúa en un mundo posapocalíptico y está protagonizada por Rick Grimes donde pelean con muertos vivientes.%BOLIVISION*
			  Medium%22:00 pm%Esta ficción de misterio está basada en la vida real de la médium Allison DuBois, interpretada por Patricia Arquette, cuyo don le ha permitido en su corta carrera resolver muchos casos para la justicia.%BOLIVISION*
			  Sex and the city%23:00 pm%La serie trata acerca de las vidas y amoríos de cuatro mujeres que son muy buenas amigas, tres de las cuales están en el final de sus treinta años Carrie, Charlotte y Miranda y otra Samantha que está en los cuarenta.%BOLIVISION*
			  El mentalista%23:30 pm%Patrick Jane Simon Baker es un famoso médium que se gana la vida apareciendo en varios programas de televisión. Su vida cambia por completo cuando un asesino conocido como John el Rojo Red John, en inglés acaba con las vidas de su esposa y de su hija.%UNITEL*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 38 --------------
(defrule rule38
	 ?p1 <- (SERIES)
	 ?p2 <- (NO_ANIMADAS)
	 ?p3 <- (ACCION_FICCION)
	 ?p4 <- (DOMINGO)
	 =>
	 (assert (Last resort%22:00 pm%Last Resort es una serie de televisión estadounidense dramática militar creada por Shawn Ryan y Karl Gajdusek%ATB*
			  Extant%18:00 pm%Serie de ciencia ficción emitida por la cadena CBS. La serie gira en torno a una astronauta que vuelve a casa.%BOLIVISION*
			  Ncis los angeles%19:00 pm%es una serie de televisión de ficción estadounidense sobre un equipo de agentes especiales del Servicio de Investigación Criminal de la Marina de los Estados Unidos que trabajan en Los Ángeles.%BOLIVISION*
			  Hawaii Five0%20:00 pm%Hawaii Five0 es una serie de televisión de crimen y drama estadounidense%BOLIVISION*
			  The walking dead%21:00 pm% La serie se sitúa en un mundo posapocalíptico y está protagonizada por Rick Grimes donde pelean con muertos vivientes.%BOLIVISION*
			  Medium%22:00 pm%Esta ficción de misterio está basada en la vida real de la médium Allison DuBois, interpretada por Patricia Arquette, cuyo don le ha permitido en su corta carrera resolver muchos casos para la justicia.%BOLIVISION*
			  Sex and the city%23:00 pm%La serie trata acerca de las vidas y amoríos de cuatro mujeres que son muy buenas amigas, tres de las cuales están en el final de sus treinta años Carrie, Charlotte y Miranda y otra Samantha que está en los cuarenta.%BOLIVISION*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 39 --------------
(defrule rule39
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (MUSICAL)
	 ?p4 <- (JUVENIL)
	 ?p5 <- (LUNES_VIERNES)
	 =>
	 (assert (Axesso%17:00 pm%Programa musical enfocado al Rock en todas sus representaciones, como tambien Rock Boliviano%BTV*
			  ))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 40 --------------
(defrule rule40
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (MUSICAL)
	 ?p4 <- (JUVENIL)
	 ?p5 <- (SABADO)
	 =>
	 (assert (Akiba TV%15:30 pm%Programa musical enfocado al manga y anime%TVU*
			  Play Music%16:30 pm%Programa musical con todo lo nuevo en Electronica y RB%TVU*
			  ))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 41 --------------
(defrule rule41
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (MUSICAL)
	 ?p4 <- (MAYORES)
	 ?p5 <- (SABADO)
	 =>
	 (assert (En concierto%08:00 am%Musical en concierto de musica clasica%TVU*
			  Exitos del recuero%09:00 am%Programa musical de los exitos del recuerdo.%TVU*
			  La pachanga%14:00 pm, 21:00 pm%Musica variada nacional e internacional.%CANAL 2*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 42 --------------
(defrule rule42
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COMPETENCIA)
	 ?p4 <- (DEPORTES_C)
	 ?p5 <- (LUNES_VIERNES)
	 =>
	 (assert (Calle 7%18:00 pm%Programa competitivo de diferentes pruebas deportivas para los participantes.%UNITEL*
			  SKP%16:50 pm%Competencias, juegos y demas entretenimiento por ATB%ATB*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 43 --------------
(defrule rule43
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COMPETENCIA)
	 ?p4 <- (DEPORTES_C)
	 ?p5 <- (SABADO_DOMINGO)
	 =>
	 (assert (Fear Factor%15:00 pm%Una de las series de competencia mas bizarras y entretenidas de la TV.%BOLIVISION*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 44 --------------
(defrule rule44
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COMPETENCIA)
	 ?p4 <- (MUSICA_DANZA)
	 ?P5 <- (LUNES_VIERNES)
	 =>
	 (assert (Showmatch%23:00 pm%Showmatch es un programa de entretenimiento y humor argentino, presentado por Marcelo Tinelli.%ATB* 
			  Cantando por un sueño%21:00 pm%Programa de competencia de baile por cumplir sueno de una persona%RED UNO*
			  Academy Star%22:00 pm%Nuevo programa reality musical transmitido por UNITEL%UNITEL*
			  Pequenos gigantes%18:00 pm%Competencia de canto de niños.%RED UNO*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 45 --------------
(defrule rule45
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COCINA_HUMOR)
	 =>
	 (assert (La batidora%09:50 am%Cocina y humor en la casa%UNITEL*
			  Tele amiga%10:00 am%Cocina y humor en la casa%TELE C*
			  La justa%14:30%Cocina y humor en la casa%BTV*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 45 --------------
(defrule rule45
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COCINA_HUMOR)
	 =>
	 (assert (La batidora%09:50 am%Cocina y humor en la casa%UNITEL*
			  Tele amiga%10:00 am%Cocina y humor en la casa%TELE C*
			  La justa%14:30 pm%Cocina y humor en la casa%BTV*
			  Ella y el%09:50 am%Cocina y humor en la cocina%UNIVALLE*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 46 --------------
(defrule rule46
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (COCINA_HUMOR)
	 =>
	 (assert (La batidora%09:50 am%Cocina y humor en la casa%UNITEL*
			  Tele amiga%10:00 am%Cocina y humor en la casa%TELE C*
			  La justa%14:30%Cocina y humor en la casa%BTV*))
)
;-------------------------------------
;-------------------------------------
;-------------- REGLA 47 --------------
(defrule rule47
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (ENTRETENIMIENTO_VARIADO)
	 ?p4 <- (LUNES_VIERNES)
	 =>
	 (assert (Saber y ganar%16:50 pm%Entretenimiento y conocimiento saber y ganar por TVU%TVU*
			  Laura de todos%05:10 am%Laura y su reality de confrontaciones en Bolivision%BOLIVISION*
			  Caso cerrado%17:30 pm%La Dra. Polo presenta casos conmovedores con los que podrás identificarte, tomar partido y ser testigo de soluciones basadas en la verdad%BOLIVISION*
			  Buenas ideas%09:00 am%Programa televisivo que muestra la realidad de nuestras universidades.%BTV*
			  Bigote%16:00 pm%Entretenido programa televisivo con juegos, entrevistas y mucho humor%RED UNO*))
)
;-------------------------------------
;-------------- REGLA 48 --------------
(defrule rule48
	 ?p1 <- (SERIES)
	 ?p2 <- (EN_VIVO)
	 ?p3 <- (ENTRETENIMIENTO_VARIADO)
	 ?p4 <- (SABADO_DOMINGO)
	 =>
	 (assert (Doce Corazones%23:00 pm%12 corazones es un dating show que actualmente le pertenece a la cadena de Telemundo.%RED UNO* 
		      Mil maneras de morir%22:00 pm%El programa recrea muertes inusuales basadas en hechos reales o leyendas urbanas con humor negro, que incluye entrevistas con expertos que explican las muertes de las personas en cada episodio%UNITEL*))
)
;-------------------------------------
;--------------SERIES------------------

;--------------PELICULAS------------------
;--------------REGLA 49---------------
(defrule rule49
	 ?p1 <- (PELICULAS)
	 ?p2 <- (SABADO)
	 =>
	 (assert (Senor cine%21:30 pm%Las mejores peliculas por UNITEL%UNITEL*
			  ATB de película premier%16:00 pm%Peliculas por ATB%ATB*	
			  ))
)
;--------------REGLA 50-----------------
;-------------------------------------
(defrule rule50
	 ?p1 <- (PELICULAS)
	 ?p2 <- (SABADO)
	 =>
	 (assert (Senor cine%21:30 pm%Las mejores peliculas por UNITEL%UNITEL*
			  ATB de película premier%16:00 pm%Peliculas por ATB%ATB*	
			  ))
)
;-------------------------------------
;--------------REGLA 51---------------
;-------------------------------------
(defrule rule51
	 ?p1 <- (PELICULAS)
	 ?p2 <- (DOMINGO)
	 =>
	 (assert (Cine UNO%23:00 pm%Peliculas estreno por RED UNO%RED UNO* 
			  Senor cine%9:00 am%Las mejores peliculas por UNITEL%UNITEL*
			  ATB de película familiar%12:00 pm%Peliculas por ATB%ATB*	
			  ATB de película impacto%14:00 pm%Peliculas por ATB%ATB*
			  ATB de película familiar%16:00 pm%Peliculas por ATB%ATB*
			  TVU Cine%23:00 pm%Peliculas por TVU%TVU*
			  Nuestro Cine%1:00 am%Peliculas nacionales por Bolivia TV%BTV*
			 ))
)
;-------------------------------------
;--------------PELICULAS--------------

;--------------MUSICALES--------------
;--------------REGLA 52-----------------
(defrule rule52
	 ?p1 <- (MUSICALES)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (ELECTRONICA)
	 =>
	 (assert (Play Music%Sabado: 16:30 pm%Todo lo mejor en cuanto a musica electronica y RvB actual%TVU*		  
			  ))
)
;-------------------------------------
;--------------REGLA 53-----------------
(defrule rule53
	 ?p1 <- (MUSICALES)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (TROPICAL)
	 =>
	 (assert (La pachanga%Sabado:21:00 pm%Musica tropical y variada por CCA%CANAL 2*	  
			  ))
)
;-------------------------------------
;--------------REGLA 54-----------------
(defrule rule54
	 ?p1 <- (MUSICALES)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (MANGA_ANIME)
	 =>
	 (assert (Akiba TV%Sabado: 15:30 pm%Musical de series de anime%TVU*	  
			  ))
)
;-------------------------------------
;--------------REGLA 56-----------------
(defrule rule56
	 ?p1 <- (MUSICALES)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (ROCK)
	 =>
	 (assert (Axesso%Lun a Vie 17:00 pm%Programa musical enfocado al Rock en todas sus representaciones, como tambien Rock Boliviano%BTV*	  
			  ))
)
;-------------------------------------

;--------------REGLA 57-----------------
(defrule rule57
	 ?p1 <- (MUSICALES)
	 ?p2 <- (MAYORES)
	 ?p3 <- (RECUERDO)
	 =>
	 (assert (En concierto%08:00 am%Musical en concierto de musica clasica%TVU*
			  Exitos del recuero%09:00 am%Programa musical de los exitos del recuerdo.%TVU*	  
			  ))
)
;-------------------------------------
;--------------REGLA 58-----------------
(defrule rule58
	 ?p1 <- (MUSICALES)
	 ?p2 <- (MAYORES)
	 ?p3 <- (TROPICAL)
	 =>
	 (assert (La pachanga%Sabado:21:00 pm%Musica tropical y variada por CCA%CANAL 2*	  
			  ))
)
;-------------------------------------

;--------------MUSICALES--------------

;--------------TELENOVELAS------------------
;--------------REGLA 59-----------------
(defrule rule59
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (ROMANTICA)
	 ?p3 <- (LUNES_VIERNES)
	 =>
	 (assert (Esperanza mia%14:00 pm%Novela romantica%ATB*
			  Luna Roja%17:50%Novela romantica%ATB*
			  Avenida Brasil%20:30%Novela romantica%ATB*
			  Sortilegio%12:00 pm%Novela romantica%BOLIVISION*
			  La rosa de guadalupe%16:30 pm%Novela romantica%BOLIVISION*
			  La intrusa%11:00 am%Novela romantica%UNITEL*
			  Que culpa tiene Fatmagul%21:00 pm%Novela romantica%UNITEL*
			  Pasion y poder%14:00 pm%Novela romantica%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 60-----------------
(defrule rule60
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (POPULACHAS)
	 ?p3 <- (LUNES_VIERNES)
	 =>
	 (assert (De que te quiero, te quiero%14:55 pm%Novela populacha de la region%BOLIVISION*
			  A que no me dejas%19:00 pm%Novela populacha de la region%BOLIVISION*
			  Antes que muera Lichita%17:50 pm%Novela populacha de la region%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 61-----------------
(defrule rule61
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (NARCONOVELA)
	 ?p3 <- (LUNES_VIERNES)
	 =>
	 (assert (El Capo III%22:00 pm%Novela con trama narcotraficantes y accion%BOLIVISION*
			  El senor de los cielos%23:30 pm%Novela con trama narcotraficantes y accion%UNITEL*
			  ))
)
;-----------------------------------
;--------------REGLA 62-----------------
(defrule rule62
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (ROMANTICA)
	 ?p3 <- (SABADO)
	 =>
	 (assert (Que culpa tiene Fatmagul%15:00 pm%Resumen de la novela de la semana%UNITEL*
			  ))
)
;-----------------------------------
;--------------REGLA 63-----------------
(defrule rule63
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (INFANTIL)
	 ?p3 <- (SABADO)
	 =>
	 (assert (Complices al rescate%06:00 am%Novela infantil%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 64-----------------
(defrule rule64
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (SABADO)
	 =>
	 (assert (Amigas y rivales%08:00 am%Novela infantil%RED UNO*
			  Rebelde%10:00 am%Novela infantil%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 65-----------------
(defrule rule65
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (INFANTIL)
	 ?p3 <- (DOMINGO)
	 =>
	 (assert (Complices al rescate%06:00 am%Novela infantil%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 66-----------------
(defrule rule66
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (DOMINGO)
	 =>
	 (assert (Amigas y rivales%08:00 am%Novela infantil%RED UNO*
			  Rebelde%10:00 am%Novela infantil%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 67-----------------
(defrule rule67
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (INFANTIL)
	 ?p3 <- (LUNES_VIERNES)
	 =>
	 (assert (Carrusel%09:00 am%Novela infantil%RED UNO*
			  ))
)
;-----------------------------------
;--------------REGLA 68-----------------
(defrule rule68
	 ?p1 <- (TELENOVELAS)
	 ?p2 <- (JUVENIL)
	 ?p3 <- (LUNES_VIERNES)
	 =>
	 (assert (Amores con trampa%10:00 am%Novela juvenil%RED UNO*
			  Floricienta%15:00 pm%Novela juvenil argentina%RED UNO*
			  ))
)
;-----------------------------------
;--------------TELENOVELAS--------------
